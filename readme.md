# Supported tags and respective `Dockerfile` links

-	[`latest`, `epel-servertools` (*docker/Dockerfile*)](https://bitbucket.org/dmglab/docker-centos-essentials/src/master/epel-servertools/Dockerfile)
-	[`vanilla` (*docker/Dockerfile*)](https://bitbucket.org/dmglab/docker-centos-essentials/src/master/vanilla/Dockerfile)
-	[`servertools` (*docker/Dockerfile*)](https://bitbucket.org/dmglab/docker-centos-essentials/src/master/servertools/Dockerfile)
-	[`epel` (*docker/Dockerfile*)](https://bitbucket.org/dmglab/docker-centos-essentials/src/master/epel/Dockerfile)

[![](https://badge.imagelayers.io/dmglab/centos:epel.svg)](https://imagelayers.io/?images=dmglab%2Fcentos:latest,dmglab%2Fcentos:epel,dmglab%2Fcentos:servertools,dmglab%2Fcentos:epel-servertools,dmglab%2Fcentos:vanilla 'Get your own badge on imagelayers.io')

# CentOS with some extras

## Based on CentOS' official Image

For further readings, take a look to [centos](https://hub.docker.com/_/centos/)

## This is for the Devs
**These Images are not recommended for production!**

Sometimes, you want to setup a stack/app/infrastructure/whatever and you need some systemutils like `vim`, `less`, `netstat`, `openssl`, `make`, `gdb` and everything else bundled inside `infrastructure-server-environment` (with epel).

## Tags

### vanilla
centos with `yum update`

### servertools
centos with `yum group install infrastructure-server-environment`

### epel
centos with `yum install epel-release`

### epel-servertools
centos with servertools and epel